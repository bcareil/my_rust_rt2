mod data;
mod renderer;

use data::color::ColorRGB;
use data::entities::Sphere;
use data::material::Material;
use data::ray::Ray;
use rayon::prelude::*;
#[cfg(gui)]
use renderer::gui::GuiRenderer;
use renderer::ppm::PpmRenderer;
use renderer::traits::Renderer;
use std::{cmp::Ordering, f32::consts::PI};
use vecmat::vec::*;

const RES_SCALE: usize = 1;
const RES_W: usize = 320 * RES_SCALE;
const RES_H: usize = 240 * RES_SCALE;
const MIN_CONTRIBUTION: f32 = 1.0 / (2.0 * 256.0);
const MIN_DEPTH: u32 = 3;
const CONTRIBUTION_STEP_FACTOR: f32 = 0.99;
const AMBIANT_COLOR_FACTOR: f32 = 0.05;

struct Camera {
    look_at: Vec3<f32>,
    vertical: Vec3<f32>,
    position: Vec3<f32>,
    resolution: Vec2<usize>,
    near: f32,
    fov: f32,
    msaa: usize,
}

struct KernelContext<'a> {
    tex0: &'a Vec<ColorRGB>,
    tex0_dim: Vec2<i32>,
    tex1: &'a Vec<ColorRGB>,
    tex1_dim: Vec2<i32>,
}

impl<'a> KernelContext<'a> {
    pub fn new(
        tex0: &'a Vec<ColorRGB>,
        tex1: &'a Vec<ColorRGB>,
        w: usize,
        h: usize,
    ) -> KernelContext<'a> {
        let dim = Vec2::<i32> {
            data: [w as i32, h as i32],
        };
        KernelContext {
            tex0,
            tex0_dim: dim,
            tex1,
            tex1_dim: dim,
        }
    }

    fn clamp(xy: Vec2<i32>, dim: Vec2<i32>) -> Vec2<i32> {
        let [x, y] = xy.data;
        let [w, h] = dim.data;
        let x = x.clamp(0, w - 1);
        let y = y.clamp(0, h - 1);
        Vec2::<i32> { data: [x, y] }
    }

    fn to_index(xy: Vec2<i32>, dim: Vec2<i32>) -> usize {
        let [x, y] = xy.data;
        let w = dim[0] as usize;
        let x = x as usize;
        let y = y as usize;
        x + y * w
    }

    pub fn pixel(&self, xy: Vec2<i32>) -> ColorRGB {
        self.pixel_t0(xy)
    }

    pub fn pixel_t0(&self, xy: Vec2<i32>) -> ColorRGB {
        let idx = KernelContext::to_index(KernelContext::clamp(xy, self.tex0_dim), self.tex0_dim);
        self.tex0[idx]
    }

    pub fn pixel_t1(&self, xy: Vec2<i32>) -> ColorRGB {
        let idx = KernelContext::to_index(KernelContext::clamp(xy, self.tex1_dim), self.tex1_dim);
        self.tex1[idx]
    }
}

fn kernel_extract_overflow(ctx: &KernelContext, xy: Vec2<i32>) -> ColorRGB {
    ctx.pixel(xy).overflow()
}

fn kernel_add(ctx: &KernelContext, xy: Vec2<i32>) -> ColorRGB {
    ctx.pixel_t0(xy) + ctx.pixel_t1(xy)
}

macro_rules! kernel_gauss_blur_gen {
    ($fname: ident, $dir_v: expr) => {
        fn $fname(ctx: &KernelContext, xy: Vec2<i32>) -> ColorRGB {
            const SPAN: i32 = 8;
            const SIGMA: f32 = 2.0;
            const SIGMA_SQ: f32 = SIGMA * SIGMA;
            const SIGMA_SQ_2: f32 = SIGMA_SQ * 2.0;
            const RCPR_SIGMA_SQ_2: f32 = 1.0 / SIGMA_SQ_2;
            let mut gauss = Vec3::<f32> {
                data: [1.0, (-RCPR_SIGMA_SQ_2).exp(), 0.0],
            };
            gauss[2] = gauss[1] * gauss[1];
            let mut p = ctx.pixel(xy);
            let mut acc: f32 = 0.0;
            gauss[0] *= gauss[1];
            gauss[1] *= gauss[2];
            let dir_v = $dir_v;
            let mut dir = Vec2::<i32>::new();
            for _ in 1..SPAN {
                dir += dir_v;
                let pl = ctx.pixel(xy + dir);
                let pr = ctx.pixel(xy - dir);
                p = p + (pr + pl) * gauss[0];
                acc += gauss[0];
                gauss[0] *= gauss[1];
                gauss[1] *= gauss[2];
            }
            acc = 1.0 + acc * 2.0;
            p / acc
        }
    };
}

kernel_gauss_blur_gen!(kernel_gauss_blur_v, Vec2::<i32>::from(0, 1));
kernel_gauss_blur_gen!(kernel_gauss_blur_h, Vec2::<i32>::from(1, 0));

fn run_kernel(
    ctx: &KernelContext,
    buffer: &mut [ColorRGB],
    kernel: fn(&KernelContext, Vec2<i32>) -> ColorRGB,
) {
    buffer
        .par_chunks_mut(ctx.tex0_dim[0] as usize)
        .enumerate()
        .for_each(|(y, row)| {
            row.iter_mut()
                .enumerate()
                .for_each(|(x, p)| *p = kernel(ctx, Vec2::<i32>::from(x as i32, y as i32)));
        });
}

pub fn float_cmp_ignore_spec(a: f32, b: f32, e: f32) -> Ordering {
    let tmp = (a - b).abs();
    if tmp < e {
        Ordering::Equal
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}

fn intersect(r: &Ray, s: &Sphere) -> Option<f32> {
    let center_to_org = r.src - s.center;
    let center_to_org_dist_sq = center_to_org.sqrlen();
    // solve quadratic formula: ax^2 + bx + c = 0 where
    // a = ||dir||^2 = 1.0 // (dir is a unit vector)
    // b = 2 (dir . (org - center))
    // c = ||org - center||^2 - radius^2
    // delta = b^2 - 4ac
    let radius_sq = s.radius * s.radius;
    let b = 2.0 * r.dir.dot(center_to_org);
    let c = center_to_org_dist_sq - radius_sq;
    let delta = (b * b) - (4.0 * c);

    match float_cmp_ignore_spec(delta, 0.0, 0.001) {
        // delta < 0 => no solution
        Ordering::Less => None,
        // delta = 0 => solution x = -b / 2a
        Ordering::Equal => {
            if b < 0.0 {
                Some(-b / 2.0)
            } else {
                None
            }
        }
        // delta > 0 => two solutions
        Ordering::Greater => {
            let delta_sqrt = delta.sqrt();
            let k1 = (-delta_sqrt - b) / 2.0;
            let k2 = (delta_sqrt - b) / 2.0;
            if k1 < k2 && k1 > 1.0 {
                Some(k1)
            } else {
                Some(k2)
            }
        }
    }
}

fn intersect_with_closest(r: &Ray, spheres: &Vec<Sphere>) -> Option<(usize, f32)> {
    let mut closest: Option<(usize, f32)> = None;
    for i in 0..spheres.len() {
        match intersect(r, &spheres[i]) {
            Some(distance) => {
                if distance > 0.0 {
                    match closest {
                        None => {
                            closest = Some((i, distance));
                        }
                        Some((_, other_distance)) => {
                            if distance < other_distance {
                                closest = Some((i, distance));
                            }
                        }
                    }
                }
            }
            None => {}
        }
    }
    closest
}

struct IntersectionContext<'a> {
    ray: &'a Ray,
    sphere: &'a Sphere,
    going_in: bool,
    normal: Vec3<f32>,
    intersection_below: Vec3<f32>,
    intersection_above: Vec3<f32>,
}

impl<'a> IntersectionContext<'a> {
    fn new(
        ray: &'a Ray,
        sphere: &'a Sphere,
        _index: usize,
        distance: f32,
    ) -> IntersectionContext<'a> {
        let intersection = ray.src + ray.dir * distance;
        let normal = (intersection - sphere.center).normalize();
        let offset = normal * 0.001;
        let intersection_above = intersection + offset;
        let intersection_below = intersection - offset;
        let going_in = normal.dot(ray.dir) < 0.0;
        let (intersection_above, intersection_below, normal) = if going_in {
            (intersection_above, intersection_below, normal)
        } else {
            (intersection_below, intersection_above, normal * -1.0)
        };

        IntersectionContext {
            ray,
            sphere,
            going_in,
            normal,
            intersection_below,
            intersection_above,
        }
    }
}

fn calc_refracted_color(
    intr: &IntersectionContext,
    spheres: &Vec<Sphere>,
    contribution: f32,
    depth: u32,
    ri_stack: &mut Vec<f32>,
) -> ColorRGB {
    let m = &intr.sphere.material;
    let c = match intr.ray.refract(
        &intr.intersection_below,
        &intr.normal,
        m.refractive_index,
        ri_stack,
    ) {
        Some(transpercing_ray) => {
            if !intr.going_in {
                launch_ray(
                    &transpercing_ray,
                    spheres,
                    contribution,
                    depth + 1,
                    ri_stack,
                )
            } else {
                match intersect_with_closest(&transpercing_ray, spheres) {
                    None => m.diffuse_color,
                    Some((_os, oth_dist)) => {
                        let go_through_chance = 1.0 - m.translucent_factor * oth_dist;
                        //dbg!(go_through_chance, m.translucent_factor, oth_dist);
                        let dice = rand::random::<f32>();
                        if dice < go_through_chance || oth_dist > (2.0 * intr.sphere.radius) {
                            launch_ray(
                                &transpercing_ray,
                                spheres,
                                contribution,
                                depth + 1,
                                ri_stack,
                            )
                        } else {
                            let d1 = rand::random::<f32>();
                            let d2 = rand::random::<f32>();
                            let x = if d1 < 0.5 { -1.0 } else { 1.0 };
                            let normal = Vec3::<f32>::from(x, 0.0, 0.0);
                            let new_src =
                                transpercing_ray.src + transpercing_ray.dir * (d2 * oth_dist);
                            let sss_ray =
                                transpercing_ray.randomize_along_normal(&new_src, &normal, 0.0);
                            let c =
                                launch_ray(&sss_ray, spheres, contribution, depth + 0, ri_stack);
                            c * m.diffuse_color
                        }
                    }
                }
            }
        }
        None => {
            let reflected_ray = intr.ray.reflect(&intr.intersection_above, &intr.normal);
            let specular_color =
                launch_ray(&reflected_ray, spheres, contribution, depth + 1, ri_stack);
            specular_color
        }
    };
    c * m.transparent_factor
}

fn launch_ray(
    r: &Ray,
    spheres: &Vec<Sphere>,
    contribution: f32,
    depth: u32,
    ri_stack: &mut Vec<f32>,
) -> ColorRGB {
    if depth > MIN_DEPTH && (0.1 + rand::random::<f32>()) > contribution {
        return ColorRGB::black();
    }

    match intersect_with_closest(r, spheres) {
        Some((index, distance)) => {
            let intr = IntersectionContext::new(r, &spheres[index], index, distance);

            //if r.pixel[0] == 110 && r.pixel[1] == 20 {
            //    println!("from {:?} dir {:?} contrib {} depth {} hit {} at {:?}|{:?}", r.src, r.dir, contribution, depth, index, intersection_above, intersection_below);
            //}

            let contribution = contribution * CONTRIBUTION_STEP_FACTOR;
            let m = &intr.sphere.material;
            let light_factor = m.light_color.sum() / 3.0;

            let transparent_contribution = m.transparent_factor * contribution;
            let transparent_color = if transparent_contribution > MIN_CONTRIBUTION {
                calc_refracted_color(&intr, spheres, transparent_contribution, depth, ri_stack)
            } else {
                ColorRGB::black()
            };

            let reflective_contribution = contribution * m.reflective_factor;
            let specular_color = if reflective_contribution > MIN_CONTRIBUTION {
                let reflected_ray = r.reflect(&intr.intersection_above, &intr.normal);
                let specular_color = if m.gloss_factor > 0.0 && m.gloss_factor < 1.0 {
                    let reflected_ray = reflected_ray.randomize_along_normal(
                        &intr.intersection_above,
                        &reflected_ray.dir,
                        m.gloss_factor,
                    );

                    launch_ray(
                        &reflected_ray,
                        spheres,
                        reflective_contribution,
                        depth + 1,
                        ri_stack,
                    )
                } else {
                    launch_ray(
                        &reflected_ray,
                        spheres,
                        reflective_contribution,
                        depth + 1,
                        ri_stack,
                    )
                };
                specular_color * m.specular_color * m.reflective_factor
            } else {
                ColorRGB::black()
            };

            let diffuse_color_factor =
                1.0 - (m.transparent_factor + m.reflective_factor + light_factor);
            let ambiant_color = if diffuse_color_factor > MIN_CONTRIBUTION {
                m.ambiant_color * AMBIANT_COLOR_FACTOR
            } else {
                ColorRGB::black()
            };

            let diffuse_color =
                if diffuse_color_factor > MIN_CONTRIBUTION && m.diffuse_color.sum() > 0.0 {
                    let diffuse_ray =
                        r.randomize_along_normal(&intr.intersection_above, &intr.normal, 0.0);
                    let gathered_color = launch_ray(
                        &diffuse_ray,
                        spheres,
                        contribution * diffuse_color_factor,
                        depth + 1,
                        ri_stack,
                    );
                    if gathered_color.sum() > 0.001 {
                        m.diffuse_color * gathered_color * diffuse_color_factor
                    } else {
                        ColorRGB::black()
                    }
                } else {
                    ColorRGB::black()
                };

            let final_color =
                ambiant_color + diffuse_color + transparent_color + specular_color + m.light_color;
            final_color
        }
        None => ColorRGB::new(1.0, 1.0, 0.0),
    }
}

fn deg_to_rad(a: f32) -> f32 {
    a * PI / 180.0
}

fn compute_rays(camera: &Camera) -> Vec<Ray> {
    let msaa = camera.msaa;
    let msaa_r = msaa.trailing_zeros() as usize;
    let msaa_rh = msaa_r / 2;
    let res_w = camera.resolution[0];
    let res_h = camera.resolution[1];
    let ratio = res_w as f32 / (res_h as f32);
    let fov = camera.fov;

    let mut rays = vec![
        Ray {
            dir: Vec3::<f32>::zero(),
            src: Vec3::<f32>::zero(),
            refractive_index: 1.0,
            pixel: Vec2::<i32>::zero()
        };
        res_w * res_h * msaa
    ];

    let look_at_dir = (camera.look_at - camera.position).normalize();
    let vertical = camera.vertical.normalize();
    let horizontal = look_at_dir.cross(camera.vertical);

    let w = (0.5 * fov).tan() * (2.0 * camera.near);
    let h = w / ratio;
    let x_fract: f32 = w / (res_w as f32);
    let y_fract: f32 = h / (res_h as f32);
    let x_ms_fract: f32 = x_fract / ((1 + msaa_r) as f32);
    let y_ms_fract: f32 = y_fract / ((1 + msaa_r) as f32);
    //dbg!(msaa, msaa_r, msaa_rh);
    //dbg!(fov, camera.near, w, h);
    //dbg!(x_fract, y_fract, x_ms_fract, y_ms_fract);
    let mut i = 0;
    for msaa_y in 0..msaa_r {
        let h_start = h + (0.5 + (msaa_rh as f32) - (msaa_y as f32)) * y_ms_fract;
        for msaa_x in 0..msaa_r {
            let w_start = w + (0.5 + (msaa_rh as f32) - (msaa_x as f32)) * x_ms_fract;
            //dbg!(h, h_start, w, w_start);
            let mut y = 0.5 * h_start;
            for py in 0..res_h {
                let v = y;
                y -= y_fract;
                let mut x = -0.5 * w_start;
                for px in 0..res_w {
                    let u = x;
                    x += x_fract;

                    let vec = (look_at_dir * camera.near) + (u * horizontal) + (v * vertical);
                    rays[i].src = vec + camera.position;
                    rays[i].dir = vec.normalize();
                    rays[i].pixel = Vec2::<i32>::from(px as i32, py as i32);
                    i += 1;
                }
            }
        }
    }

    rays
}

#[allow(dead_code)]
fn load_scene() -> Vec<Sphere> {
    let mut spheres = vec![];

    for _ in 0..5 {
        let h = rand::random::<f32>();
        let s = 0.5f32;
        let v = rand::random::<f32>();
        let v: f32 = 0.8 + v * 0.1;
        let color = ColorRGB::from_hsv(h, s, v);

        let mut reflection = rand::random::<f32>();
        reflection = f32::max(reflection - 0.7, 0.0);
        if reflection > 0.0 {
            reflection = f32::min(0.5 + reflection * 2.0, 1.0);
        }

        let mut transparency = rand::random::<f32>();
        transparency = f32::max(transparency - 0.7, 0.0);
        if transparency > 0.0 {
            transparency = f32::min(0.5 + transparency, 1.0);
        }

        let refractive_index = if transparency > 0.0 {
            let index = rand::random::<f32>() - 0.5;
            if index > 0.0 {
                index + 1.1
            } else {
                0.0
            }
        } else {
            0.0
        };

        if reflection + transparency > 1.0 {
            let total = reflection + transparency;
            reflection /= total;
            transparency /= total;
        }

        let material = Material {
            specular_color: ColorRGB::white(),
            diffuse_color: color,
            ambiant_color: color,
            light_color: ColorRGB::black(),
            reflective_factor: reflection,
            transparent_factor: transparency,
            translucent_factor: 1.0,
            refractive_index,
            gloss_factor: 0.0,
        };

        let mut x = rand::random::<f32>();
        let mut y = rand::random::<f32>();
        let mut r = rand::random::<f32>();
        x = x * 4.0 - 2.0;
        y = y * 4.0 - 2.0;
        r = r * r / 2.0 + 0.1;
        let sphere = Sphere {
            center: Vec3::<f32>::from(x, y, r),
            radius: r,
            material,
        };
        spheres.push(sphere);
    }

    spheres
}

#[allow(dead_code)]
fn load_scene_1() -> (Camera, Vec<Sphere>) {
    let mut spheres = vec![];

    let camera = Camera {
        look_at: Vec3::<f32>::from(0.0, 0.0, 0.0),
        vertical: Vec3::<f32>::from(0.0, 0.0, 1.0),
        position: Vec3::<f32>::from(-3.0, -3.0, 2.5),
        resolution: Vec2::<usize>::from(RES_W, RES_H),
        near: 1.0,
        fov: deg_to_rad(70.0),
        msaa: 4,
    };

    let reflection: f32 = 0.2;
    let radius: f32 = 0.5;

    let mat1 = Material::new()
        .glossy(reflection, 0.8, ColorRGB::white())
        .diffuse(ColorRGB::new(1.0, 0.2, 0.2));
    let s1 = Sphere {
        center: Vec3::<f32>::from(1.0, 0.0, radius),
        radius,
        material: mat1,
    };
    spheres.push(s1);

    let mat2 = Material::new()
        .specular(reflection, ColorRGB::white())
        .diffuse(ColorRGB::new(0.2, 1.0, 0.2));
    let s2 = Sphere {
        center: Vec3::<f32>::from(0.0, 1.0, radius),
        radius,
        material: mat2,
    };
    spheres.push(s2);

    let radius = 0.4;
    let mat3 = Material::new().translucent(1.0, 2.4, radius * 5.0, ColorRGB::new(0.2, 0.2, 1.0));
    let s3 = Sphere {
        center: Vec3::<f32>::from(-1.0, -1.0, 0.4 + radius),
        radius,
        material: mat3,
    };
    spheres.push(s3);

    //let radius = 0.1;
    //let mat4 = Material::new()
    //    .specular(0.1, ColorRGB::white())
    //    .translucent(0.8, 0.4, 0.5, ColorRGB::new(0.2, 1.0, 1.0));
    //let s4 = Sphere {
    //    center: Vec3::<f32>::from(-1.0, -1.0, 0.8),
    //    radius,
    //    material: mat4,
    //};
    //spheres.push(s4);

    // light
    let radius = 0.2;
    let mat = Material::new().light(ColorRGB::gray(3.0));
    let s = Sphere {
        center: Vec3::<f32>::from(0.0, 0.0, radius),
        radius,
        material: mat,
    };
    spheres.push(s);

    // wall
    let radius = 1000.0;
    let centers = vec![
        (3.0 + radius, 0.0, 0.0),
        (-3.0 - radius, 0.0, 0.0),
        (0.0, 3.0 + radius, 0.0),
        (0.0, -3.0 - radius, 0.0),
        (0.0, 0.0, 3.0 + radius),
        (0.0, 0.0, -0.5 - radius),
    ];
    let diffuse = vec![
        ColorRGB::new(0.6, 0.6, 1.0),
        ColorRGB::new(1.0, 1.0, 0.6),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
    ];
    let light_levels = vec![0.0, 0.0, 0.0, 0.0, 1.0, 0.0];
    for i in 0..6 {
        let mat = Material::new()
            .diffuse(diffuse[i])
            .light(ColorRGB::gray(light_levels[i]));
        let center = centers[i];
        let s = Sphere {
            center: Vec3::<f32>::from(center.0, center.1, center.2),
            radius,
            material: mat,
        };
        spheres.push(s);
    }

    (camera, spheres)
}

#[allow(dead_code)]
fn load_scene_2() -> (Camera, Vec<Sphere>) {
    let mut spheres = vec![];

    let camera = Camera {
        look_at: Vec3::<f32>::from(0.0, 0.0, 0.0),
        vertical: Vec3::<f32>::from(0.0, 0.0, 1.0),
        position: Vec3::<f32>::from(0.0, -5.0, 0.0),
        resolution: Vec2::<usize>::from(RES_W, RES_H),
        near: 1.0,
        fov: deg_to_rad(70.0),
        msaa: 4,
    };

    // back
    let radius: f32 = 1.0;
    let mat = Material::new().diffuse(ColorRGB::new(0.1, 0.1, 1.0));
    let s = Sphere {
        center: Vec3::<f32>::from(2.0, 0.0, 0.0),
        radius,
        material: mat,
    };
    spheres.push(s);

    // front
    let radius: f32 = 0.4;
    let mat = Material::new().translucent(0.91, 5.6, 0.6, ColorRGB::white());
    let s = Sphere {
        center: Vec3::<f32>::from(-1.0, 0.0, 0.0),
        radius,
        material: mat,
    };
    spheres.push(s);

    // light
    let radius: f32 = 0.2;
    let mat = Material::new().light(ColorRGB::gray(0.8));
    let s = Sphere {
        center: Vec3::<f32>::from(0.0, 0.0, 0.3),
        radius,
        material: mat,
    };
    spheres.push(s);

    (camera, spheres)
}

#[allow(dead_code)]
fn load_scene_3() -> (Camera, Vec<Sphere>) {
    let mut spheres = vec![];

    let camera = Camera {
        look_at: Vec3::<f32>::from(0.0, 0.0, 0.0),
        vertical: Vec3::<f32>::from(0.0, 0.0, 1.0),
        position: Vec3::<f32>::from(-3.0, -3.0, 2.5),
        resolution: Vec2::<usize>::from(RES_W, RES_H),
        near: 1.0,
        fov: deg_to_rad(70.0),
        msaa: 4,
    };

    let radius = 1.0;
    let mat3 = Material::new().translucent(1.0, 0.8, radius * 3.0, ColorRGB::new(1.0, 0.4, 0.6));
    let s3 = Sphere {
        center: Vec3::<f32>::from(1.3, 1.1, -1.2),
        radius,
        material: mat3,
    };
    spheres.push(s3);

    // wall
    let radius = 1000.0;
    let offset = 3.0;
    let centers = vec![
        (1.0, 0.0, 0.0),
        (-1.0, 0.0, 0.0),
        (0.0, 1.0, 0.0),
        (0.0, -1.0, 0.0),
        (0.0, 0.0, 1.0),
        (0.0, 0.0, -1.0),
    ];
    let diffuse = vec![
        ColorRGB::new(0.6, 0.6, 1.0),
        ColorRGB::new(1.0, 1.0, 0.6),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
        ColorRGB::new(1.0, 1.0, 1.0),
    ];
    let light_levels = vec![0.0, 0.0, 0.0, 0.0, 1.0, 0.0];
    for i in 0..6 {
        let mat = Material::new()
            .diffuse(diffuse[i])
            .light(ColorRGB::gray(light_levels[i]));
        let center = centers[i];
        let center = Vec3::<f32>::from(center.0, center.1, center.2);
        let center = center * (radius + offset);
        let s = Sphere {
            center,
            radius,
            material: mat,
        };
        spheres.push(s);
    }

    (camera, spheres)
}

#[allow(dead_code)]
fn load_scene_4() -> (Camera, Vec<Sphere>) {
    let mut spheres = vec![];

    let camera = Camera {
        look_at: Vec3::<f32>::from(0.0, 0.0, 0.0),
        vertical: Vec3::<f32>::from(0.0, 0.0, 1.0),
        position: Vec3::<f32>::from(-3.0, -3.0, 2.5),
        resolution: Vec2::<usize>::from(RES_W, RES_H),
        near: 1.0,
        fov: deg_to_rad(70.0),
        msaa: 4,
    };

    let radius = 0.8;
    let mat3 = Material::new()
        .diffuse(ColorRGB::new(0.4, 0.4, 1.0))
        .glossy(0.2, 0.8, ColorRGB::white());
    let s3 = Sphere {
        center: Vec3::<f32>::from(-1.3, -1.1, -0.8),
        radius,
        material: mat3,
    };
    spheres.push(s3);

    let radius = 0.8;
    let mat3 = Material::new().diffuse(ColorRGB::new(0.4, 1.0, 0.6));
    let s3 = Sphere {
        center: Vec3::<f32>::from(-1.3, 1.1, -1.2),
        radius,
        material: mat3,
    };
    spheres.push(s3);

    let radius = 1.0;
    let mat3 = Material::new().translucent(1.0, 1.8, radius * 3.0, ColorRGB::new(1.0, 0.4, 0.6));
    let s3 = Sphere {
        center: Vec3::<f32>::from(1.3, 1.1, -1.2),
        radius,
        material: mat3,
    };
    spheres.push(s3);

    // light
    let radius = 1.0;
    let mat = Material::new().light(ColorRGB::gray(10.0));
    let s = Sphere {
        center: Vec3::<f32>::from(0.0, 0.0, 3.5),
        radius,
        material: mat,
    };
    spheres.push(s);

    // wall
    let radius = 4.0;
    let mat = Material::new()
        .specular(0.8, ColorRGB::white())
        .diffuse(ColorRGB::white());
    let s = Sphere {
        center: Vec3::<f32>::from(0.0, 0.0, 0.0),
        radius,
        material: mat,
    };
    spheres.push(s);

    (camera, spheres)
}

fn process_row(
    msidx: usize,
    camera: &Camera,
    row: usize,
    buffer: &mut [ColorRGB],
    rays: &Vec<Ray>,
    spheres: &Vec<Sphere>,
) {
    let res_w = camera.resolution[0];
    let res_h = camera.resolution[1];

    let y = row * res_w;
    let base_ray_idx = y + msidx * res_w * res_h;

    let mut ri_stack: Vec<f32> = vec![];
    for x in 0..res_w {
        buffer[x] = launch_ray(&rays[base_ray_idx + x], &spheres, 1.0, 0, &mut ri_stack);
    }
}

fn process(camera: &Camera, spheres: &Vec<Sphere>, renderer: &mut dyn Renderer) -> Vec<ColorRGB> {
    let res_w = camera.resolution[0];
    let res_h = camera.resolution[1];
    let rays: Vec<_> = compute_rays(&camera);

    let buffer_count = 8usize;
    let iteration_count = 1usize << (buffer_count - 1usize);
    let mut buffers: Vec<Vec<ColorRGB>> = vec![vec![]; buffer_count];
    buffers
        .iter_mut()
        .for_each(|b| b.resize(res_w * res_h, ColorRGB::black()));

    let mut last_update = std::time::Instant::now();
    for iteration in 0..iteration_count {
        let b_idx = iteration.count_ones() as usize;
        let buffer: &mut [ColorRGB] = &mut buffers[b_idx];

        //println!("Iter {} writing into {}", iteration, b_idx);

        buffer.par_chunks_mut(res_w).enumerate().for_each(|chunk| {
            process_row(
                iteration % camera.msaa,
                &camera,
                chunk.0,
                chunk.1,
                &rays,
                &spheres,
            )
        });

        let next_b_idx = (iteration + 1).count_ones() as usize;
        if b_idx >= next_b_idx {
            for src_idx in (next_b_idx..=b_idx).rev() {
                let dst_idx = src_idx - 1;
                let mut i = buffers[dst_idx..=src_idx].iter_mut();
                let dst = i.next().unwrap();
                let src = i.next().unwrap();
                //println!("Merging {} into {}", src_idx, dst_idx);
                dst.par_iter_mut()
                    .zip(src.par_iter())
                    .chunks(res_w)
                    .for_each(|r| {
                        for p in r {
                            *p.0 = (*p.0 + *p.1) / 2.0
                        }
                    });
            }
        }

        let now = std::time::Instant::now();
        if (now - last_update) > std::time::Duration::from_millis(200) {
            last_update = now;
            let loop_ = renderer.present_intermediate(&buffers[next_b_idx - 1], res_w, res_h);
            if !loop_ {
                return buffers[next_b_idx - 1].clone();
            }
        }
    }

    let org = &buffers[0];

    let mut buf0 = org.clone();
    let mut buf1 = org.clone();

    let kernel_ctx = KernelContext::new(org, org, res_w, res_h);
    run_kernel(&kernel_ctx, &mut buf0, kernel_extract_overflow);

    let kernel_ctx = KernelContext::new(&buf0, org, res_w, res_h);
    run_kernel(&kernel_ctx, &mut buf1, kernel_gauss_blur_h);

    let kernel_ctx = KernelContext::new(&buf1, org, res_w, res_h);
    run_kernel(&kernel_ctx, &mut buf0, kernel_gauss_blur_v);

    let kernel_ctx = KernelContext::new(&buf0, org, res_w, res_h);
    run_kernel(&kernel_ctx, &mut buf1, kernel_add);
    return buf1;
}

#[cfg(not(gui))]
fn make_renderer(_w: usize, _h: usize) -> impl Renderer {
    PpmRenderer::new("img.ppm")
}

#[cfg(gui)]
fn make_renderer(w: usize, h: usize) -> impl Renderer {
    GuiRenderer::new(res_w, res_h)
}

fn main() {
    let (camera, spheres): (Camera, Vec<_>) = load_scene_1();
    let res_w = camera.resolution[0];
    let res_h = camera.resolution[1];

    let renderer: &mut dyn Renderer = &mut make_renderer(res_w, res_h);

    let result = process(&camera, &spheres, renderer);
    renderer.present_final(&result, res_w, res_h);
}
