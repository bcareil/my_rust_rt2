use rand;
use vecmat::vec::*;

#[derive(Clone, Debug)]
pub struct Ray {
    pub src: Vec3<f32>,
    pub dir: Vec3<f32>,
    pub refractive_index: f32,
    pub pixel: Vec2<i32>,
}

impl Ray {
    #[allow(dead_code)]
    pub fn relocate(&self, new_src: Vec3<f32>, new_dir: Vec3<f32>) -> Ray {
        Ray {
            src: new_src,
            dir: new_dir,
            refractive_index: self.refractive_index,
            pixel: self.pixel,
        }
    }
    pub fn reflect(&self, new_src: &Vec3<f32>, normal: &Vec3<f32>) -> Ray {
        let new_dir = self.dir - 2.0 * self.dir.dot(*normal) * *normal;
        Ray {
            src: *new_src,
            dir: new_dir.normalize(),
            refractive_index: self.refractive_index,
            pixel: self.pixel,
        }
    }

    pub fn refract(
        &self,
        new_src: &Vec3<f32>,
        normal: &Vec3<f32>,
        obj_ri: f32,
        ri_stack: &mut Vec<f32>,
    ) -> Option<Ray> {
        let mut cosi = self.dir.dot(*normal);
        let n;
        let eta;
        let new_ri;
        if cosi < 0.0 {
            // going into the object
            cosi = -cosi;
            eta = self.refractive_index / obj_ri;
            n = *normal;
            new_ri = obj_ri;
            ri_stack.push(self.refractive_index);
        } else {
            // going out
            new_ri = match ri_stack.last() {
                None => 1.0,
                Some(x) => {
                    let t = *x;
                    ri_stack.pop();
                    t
                }
            };
            eta = new_ri / self.refractive_index;
            n = -*normal;
        }
        let k = 1.0 - (eta * eta * (1.0 - cosi * cosi));
        if k < 0.0 {
            None
        } else {
            let dir = self.dir * eta + (n * (eta * cosi - k.sqrt()));
            let dir = dir.normalize();
            //println!("refract: {:?} -> {:?}", self.dir, dir);
            Some(Ray {
                src: *new_src,
                dir,
                refractive_index: new_ri,
                pixel: self.pixel,
            })
        }
    }

    pub fn randomize_along_normal(&self, source: &Vec3<f32>, normal: &Vec3<f32>, f: f32) -> Ray {
        let dir = Ray::randomize_vec3_along_normal(normal, f);
        return Ray {
            src: *source,
            dir,
            refractive_index: self.refractive_index,
            pixel: self.pixel,
        };
    }

    pub fn randomize_vec3_along_normal(normal: &Vec3<f32>, f: f32) -> Vec3<f32> {
        use std::f32::consts::PI;
        // randomize angle in an hemisphere
        let seed0 = rand::random::<f32>();
        let seed1 = rand::random::<f32>();
        let sint = (seed0 * (1.0 - f)).sqrt();
        let cost = (1.0 - (sint * sint)).sqrt();
        let phi = seed1 * (2.0 * PI);
        let u = sint * phi.cos();
        let v = sint * phi.sin();
        // build a frame of reference around the normal
        let tu = if normal[0].abs() < 0.5 {
            normal.cross(Vec3::<f32>::from(1.0, 0.0, 0.0))
        } else {
            normal.cross(Vec3::<f32>::from(0.0, 1.0, 0.0))
        };
        let tv = normal.cross(tu);
        // final vector main axis
        let ru = tu * u;
        let rv = tv * v;
        let rn = *normal * cost;
        //dbg!(
        //    sint,
        //    cost,
        //    phi,
        //    u,
        //    v,
        //    ru + rv + rn,
        //    (ru + rv + rn).normalize()
        //);
        // final vector
        return (ru + rv + rn).normalize();
    }

    #[allow(dead_code)]
    pub fn randomize_vec3_in_sphere() -> Vec3<f32> {
        use std::f32::consts::PI;
        // randomize polar coordinate in a sphere
        let theta: f32 = (rand::random::<f32>() - 0.5) * PI;
        let phi: f32 = (rand::random::<f32>() - 0.5) * PI * 2.0;
        // convert to catesian
        let (sin_theta, cos_theta) = theta.sin_cos();
        let (sin_phi, cos_phi) = phi.sin_cos();
        let x = sin_theta * cos_phi;
        let y = sin_theta * sin_phi;
        let z = cos_theta;
        Vec3::<f32>::from(x, y, z)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn ray_with_dir(x: f32, y: f32, z: f32) -> Ray {
        Ray {
            src: Vec3::<f32>::from(0f32, 0f32, 0f32),
            dir: Vec3::<f32>::from(x, y, z).normalize(),
            refractive_index: 0f32,
            pixel: Vec2::<i32>::zero(),
        }
    }

    #[test]
    fn ray_test_reflect_simple() {
        let origin = Vec3::<f32>::from(0f32, 0f32, 0f32);
        let n1 = Vec3::<f32>::from(1f32, 0f32, 0f32);

        let r1 = ray_with_dir(1f32, 0f32, 0f32);
        let e1 = ray_with_dir(-1f32, 0f32, 0f32);
        let u1 = r1.reflect(&origin, &n1);
        assert_eq!(&u1.dir, &e1.dir);

        let r2 = ray_with_dir(-1.0, 0.0, 0.0);
        let e2 = ray_with_dir(1.0, 0.0, 0.0);
        let u2 = r2.reflect(&origin, &n1);
        assert_eq!(&u2.dir, &e2.dir);

        let r3 = ray_with_dir(0.0, 1.0, 0.0);
        let e3 = r3.clone();
        let u3 = r3.reflect(&origin, &n1);
        assert_eq!(&u3.dir, &e3.dir);
    }

    #[test]
    fn ray_test_reflect_adv() {
        let origin = Vec3::<f32>::from(0f32, 0f32, 0f32);
        let n1 = Vec3::<f32>::from(1f32, 0f32, 0f32);

        let r1 = ray_with_dir(-0.5, -0.5, 0.0);
        let e1 = ray_with_dir(0.70710677, -0.70710677, 0.0);
        let u1 = r1.reflect(&origin, &n1);
        dbg!(&r1, &e1, &u1);
        assert_eq!(&u1.dir, &e1.dir);
    }

    #[test]
    fn ray_test_randomize_along_normal() {
        let origin = Vec3::<f32>::from(0.0, 0.0, 0.0);
        let n1 = Vec3::<f32>::from(0.0, 0.0, 1.0);
        let r = ray_with_dir(1.0, 0.0, 0.0);
        for _ in 0..100 {
            let u1 = r.randomize_along_normal(&origin, &n1, 0.0);
            assert!(u1.dir.dot(n1) >= 0.0);
        }

        let n1 = Vec3::<f32>::from(1.0, 0.0, 0.0);
        for _ in 0..100 {
            let u1 = r.randomize_along_normal(&origin, &n1, 0.0);
            assert!(u1.dir.dot(n1) >= 0.0);
        }

        let n1 = Vec3::<f32>::from(1.0, 1.0, 1.0).normalize();
        for _ in 0..100 {
            let u1 = r.randomize_along_normal(&origin, &n1, 0.0);
            println!("{:?} -> {:?} ({})", n1, u1.dir, u1.dir.dot(n1));
            assert!(u1.dir.dot(n1) >= -0.101);
        }
    }

    #[test]
    fn ray_test_randomize_along_normal_adv() {
        let origin = Vec3::<f32>::from(0.0, 0.0, 0.0);
        let n1 = Vec3::<f32>::from(0.0, 0.0, 1.0);
        let r = ray_with_dir(1.0, 0.0, 0.0);
        let f = 0.0;
        //let mut file = File::create("ranf.log").expect("Failed to create log file");
        //file.write_all(b"value\n").unwrap();
        let mut min_v: f32 = 1.0;
        let mut max_v: f32 = 0.0;
        for _ in 0..1000 {
            let u1 = r.randomize_along_normal(&origin, &n1, f);
            let d = u1.dir.dot(n1);
            min_v = min_v.min(d);
            max_v = max_v.max(d);
            //file.write_all(format!("{d}\n").as_bytes()).unwrap();
        }
        println!("min: {min_v}, max: {max_v}");
        assert!(min_v < 0.1);
        assert!(max_v > 0.9);
    }
}
