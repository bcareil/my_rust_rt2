use crate::data::material::Material;
use vecmat::vec::Vec3;

#[derive(Debug, PartialEq, Clone)]
pub struct Sphere {
    pub radius: f32,
    pub center: Vec3<f32>,
    pub material: Material,
}
