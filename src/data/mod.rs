#[macro_use]
pub mod macros;

//pub mod buffer;
pub mod color;
pub mod entities;
pub mod material;
pub mod ray;
//pub mod vector;
//pub mod serialization;
