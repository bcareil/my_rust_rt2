use crate::data::color::ColorRGB;

#[derive(Debug, PartialEq, Clone)]
pub struct Material {
    pub specular_color: ColorRGB,
    pub diffuse_color: ColorRGB,
    pub ambiant_color: ColorRGB,
    pub light_color: ColorRGB,
    pub reflective_factor: f32,
    pub transparent_factor: f32,
    pub translucent_factor: f32,
    pub refractive_index: f32,
    pub gloss_factor: f32,
}

impl Material {
    pub fn new() -> Material {
        Material {
            specular_color: ColorRGB::black(),
            diffuse_color: ColorRGB::black(),
            ambiant_color: ColorRGB::black(),
            light_color: ColorRGB::black(),
            reflective_factor: 0.0,
            transparent_factor: 0.0,
            translucent_factor: 0.0,
            refractive_index: 1.0,
            gloss_factor: 0.0,
        }
    }

    pub fn specular(mut self, f: f32, c: ColorRGB) -> Material {
        self.specular_color = c;
        self.reflective_factor = f;
        self
    }

    pub fn glossy(mut self, f: f32, g: f32, c: ColorRGB) -> Material {
        self.gloss_factor = g;
        self.specular(f, c)
    }

    pub fn diffuse(mut self, c: ColorRGB) -> Material {
        self.ambiant_color = c;
        self.diffuse_color = c;
        self
    }

    pub fn transparent(mut self, f: f32, ri: f32) -> Material {
        self.refractive_index = ri;
        self.transparent_factor = f;
        self
    }

    pub fn translucent(mut self, f: f32, ri: f32, d: f32, c: ColorRGB) -> Material {
        self.translucent_factor = if d <= 0.001 { 0.0 } else { 1.0 / d };
        self.transparent(f, ri).diffuse(c)
    }

    pub fn light(mut self, c: ColorRGB) -> Material {
        self.light_color = c;
        self
    }
}
