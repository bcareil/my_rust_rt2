use std;
use std::ops::Div;

macro_rules! vector3_binary_op_s {
    ($a:ident, $op: ident, $b: ident) => (
        init_member_from_binary_op_s!(Vector3, $a, $op, $b, x, y, z)
    )
}

#[derive(Copy,Clone,Debug,PartialEq)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[allow(dead_code)]
impl Vector3 {
    pub fn zero() -> Vector3 {
        Vector3 { x: 0.0, y: 0.0, z: 0.0 }
    }

    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x: x, y: y, z: z }
    }

    /// Return the dot product of two vector
    pub fn dot(&self, oth: &Vector3) -> f32 {
        self.x * oth.x + self.y * oth.y + self.z * oth.z
    }

    /// Stricty equivalent to 0 - Vector
    pub fn opposite(self) -> Vector3 {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }

    /// Return the length squared of the vector
    pub fn length_sq(&self) -> f32 {
        self.dot(self)
    }

    /// Return the length of the vector
    pub fn length(&self) -> f32 {
        self.length_sq().sqrt()
    }

    /// Return the normalized vector
    pub fn normalize(self) -> Vector3 {
        let len = self.length();
        vector3_binary_op_s!(self, div, len)
    }
}

impl std::fmt::Display for Vector3 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

decl_op_v!(Add, add, Vector3, x, y, z);
decl_op_v!(Sub, sub, Vector3, x, y, z);
decl_op_v!(Mul, mul, Vector3, x, y, z);
decl_op_v!(Div, div, Vector3, x, y, z);

decl_op_s!(Add, add, f32, Vector3, x, y, z);
decl_op_s!(Sub, sub, f32, Vector3, x, y, z);
decl_op_s!(Mul, mul, f32, Vector3, x, y, z);
decl_op_s!(Div, div, f32, Vector3, x, y, z);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_opposite() {
        let vec = Vector3::new(1.0, 2.0, 3.0).opposite();

        assert_eq!(vec.x, -1.0);
        assert_eq!(vec.y, -2.0);
        assert_eq!(vec.z, -3.0);
    }

    #[test]
    fn test_length_sq() {
        let vec = Vector3::new(1.0, 2.0, -3.0);

        assert_eq!(14.0, vec.length_sq());
    }

    #[test]
    fn test_length() {
        let vec = Vector3::new(1.0, -8.0, 4.0);

        assert_eq!(9.0, vec.length());
    }

    #[test]
    fn test_normalize_00() {
        let vec = Vector3::new(1.0, 0.0, 0.0).normalize();

        assert_eq!(1.0, vec.x);
        assert_eq!(0.0, vec.y);
        assert_eq!(0.0, vec.z);
    }

    #[test]
    fn test_dot_00() {
        let vec = Vector3::new(1.0, -2.0, 3.0);

        assert_eq!(vec.length_sq(), vec.dot(&vec));
    }

    #[test]
    fn test_dot_01() {
        let vec_a = Vector3::new(1.0, 1.0, 0.0);
        let vec_b = Vector3::new(0.5, 2.0, 5.0);

        assert_eq!(2.5, vec_a.dot(&vec_b));
        assert_eq!(vec_b.dot(&vec_a), vec_a.dot(&vec_b));
    }
}
