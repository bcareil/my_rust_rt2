use crate::data::color::ColorRGB;
use crate::renderer::traits::Renderer;
use minifb::{Key, Window, WindowOptions};

pub struct GuiRenderer {
    window: Window,
    pixel_buffer: Vec<u32>,
}

impl GuiRenderer {
    pub fn new(width: usize, height: usize) -> GuiRenderer {
        let window_options = WindowOptions {
            borderless: false,
            resize: false,
            scale: minifb::Scale::X1,
            scale_mode: minifb::ScaleMode::Center,
            title: true,
            topmost: false,
            transparency: false,
            none: false,
        };
        GuiRenderer {
            window: Window::new("RustRt2", width, height, window_options).unwrap(),
            pixel_buffer: vec![0u32; width * height],
        }
    }

    fn to_pixel_buffer(&mut self, buffer: &[ColorRGB]) {
        self.pixel_buffer
            .iter_mut()
            .zip(buffer.iter())
            .for_each(|p| {
                *p.0 = if p.1.overflow().sum() > 0.0 {
                    p.1.bleed().to_u32_rgb()
                } else {
                    p.1.bleed().to_u32_rgb()
                }
            });
    }
}

impl Renderer for GuiRenderer {
    fn present_intermediate(
        &mut self,
        buffer: &Vec<ColorRGB>,
        width: usize,
        height: usize,
    ) -> bool {
        if !self.window.is_open() || self.window.is_key_down(Key::Escape) {
            return false;
        }

        self.to_pixel_buffer(buffer);

        self.window
            .update_with_buffer(&self.pixel_buffer, width, height)
            .unwrap();

        return true;
    }

    fn present_final(&mut self, buffer: &Vec<ColorRGB>, width: usize, height: usize) -> bool {
        self.to_pixel_buffer(buffer);
        self.window
            .update_with_buffer(&self.pixel_buffer, width, height)
            .unwrap();

        let mut last_update: std::time::Instant;
        while self.window.is_open() {
            if self.window.is_key_down(Key::Escape) {
                break;
            }

            let now = std::time::Instant::now();
            if (now - last_update) > std::time::Duration::from_millis(200) {
                last_update = now;
                self.window.update();
            }

            std::thread::sleep(std::time::Duration::from_millis(10));
        }
        false
    }
}
