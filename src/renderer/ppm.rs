use crate::data::color::ColorRGB;
use crate::renderer::traits::Renderer;
use std::fs::File;
use std::io::Write;

pub struct PpmRenderer<'a> {
    filename: &'a str,
}

impl<'a> PpmRenderer<'a> {
    pub fn new(filename: &'a str) -> PpmRenderer<'a> {
        PpmRenderer { filename }
    }

    pub fn write(
        &self,
        buffer: &Vec<ColorRGB>,
        width: usize,
        height: usize,
    ) -> Result<(), std::io::Error> {
        let mut file = File::create(self.filename)?;
        write!(file, "P3\n")?;
        write!(file, "{} {} 255\n", width, height)?;
        for c in buffer {
            let rgb = c.bleed().to_u32_rgb();
            let r = (0xff0000 & rgb) >> 16;
            let g = (0x00ff00 & rgb) >> 8;
            let b = 0x0000ff & rgb;
            write!(file, "{} {} {}\n", r, g, b)?;
        }
        Ok(())
    }
}

impl<'a> Renderer for PpmRenderer<'a> {
    fn present_intermediate(
        &mut self,
        _buffer: &Vec<ColorRGB>,
        _width: usize,
        _height: usize,
    ) -> bool {
        true
    }
    fn present_final(&mut self, buffer: &Vec<ColorRGB>, width: usize, height: usize) -> bool {
        self.write(buffer, width, height).unwrap();
        false
    }
}
