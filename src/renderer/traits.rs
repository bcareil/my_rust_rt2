use crate::data::color::ColorRGB;

pub trait Renderer {
    fn present_intermediate(&mut self, buffer: &Vec<ColorRGB>, width: usize, height: usize)
        -> bool;
    fn present_final(&mut self, buffer: &Vec<ColorRGB>, width: usize, height: usize) -> bool;
}
